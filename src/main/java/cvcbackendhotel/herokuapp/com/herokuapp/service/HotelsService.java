package cvcbackendhotel.herokuapp.com.herokuapp.service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import cvcbackendhotel.herokuapp.com.herokuapp.config.ErrorData;
import cvcbackendhotel.herokuapp.com.herokuapp.config.ErrorResponse;
import cvcbackendhotel.herokuapp.com.herokuapp.controller.HotelsController;
import cvcbackendhotel.herokuapp.com.herokuapp.dto.HotelDto;
import cvcbackendhotel.herokuapp.com.herokuapp.dto.HotelResultSearchDto;
import cvcbackendhotel.herokuapp.com.herokuapp.dto.HotelSearchDto;
import cvcbackendhotel.herokuapp.com.herokuapp.dto.RoomDto;
import cvcbackendhotel.herokuapp.com.herokuapp.dto.RoomPriceDetailDto;
import cvcbackendhotel.herokuapp.com.herokuapp.dto.RoomResultSearchDto;
import cvcbackendhotel.herokuapp.com.herokuapp.ws.client.HerokuappHotelsServiceClient;
import jdk.internal.jline.internal.Log;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Service
public class HotelsService {

	@Autowired
	private HerokuappHotelsServiceClient herokuappHotelsServiceClient;

	private static final BigDecimal COMISSION = new BigDecimal("0.30");
	public List<HotelDto> findHotelsByCityId(Integer cityId) {
		Assert.notNull(cityId, "Código da cidade não pode ser nulo!");

		final List<HotelDto> response = herokuappHotelsServiceClient.findHotelsByCityId(cityId);

		return response;
	}

	public List<HotelDto> findHotelDetailsById(Integer hotelId) {
		Assert.notNull(hotelId, "Código do hotel não pode ser nulo!");

		final List<HotelDto> response = herokuappHotelsServiceClient.findHotelDetailsById(hotelId);

		return response;
	}

	public List<HotelResultSearchDto> findHotelsTaxCalc(HotelSearchDto data) throws Exception {
		Assert.notNull(data, "Código do hotel não pode ser nulo!");
		LocalDate checkin = data.getCheckIn();
		LocalDate checkout = data.getCheckOut();
		Integer qtyAdult = data.getQtyAdult();
		Integer qtyChildren = data.getQtyChildren();
		long qtyDays = ChronoUnit.DAYS.between(checkin, checkout);
		Integer cityId = data.getCityCode();
		log.info("qtyAdult {}",qtyAdult);
		log.info("qtyChildren {}",qtyChildren);
		log.info("qtyDays {}",qtyDays);
		if(qtyDays < 0) {
			throw new Exception("CkeckIn maior que checkOut");
		}
		
		if(qtyAdult <= 0) {
			throw new Exception("Quantidade de Adulto não pode ser igual ou menor que 0 (ZERO)");
		}
		
		if(qtyChildren < 0) {
			throw new Exception("Quantidade de Crianças não pode menor que 0 (ZERO)");
		}
		
		final List<HotelDto> hotelsByCity = herokuappHotelsServiceClient.findHotelsByCityId(cityId);
		List<HotelResultSearchDto> response = new ArrayList<HotelResultSearchDto>();
		
		for(HotelDto hotel : hotelsByCity) {
			List<RoomResultSearchDto> rooms = new ArrayList<RoomResultSearchDto>();
			
			for(RoomDto room : hotel.getRooms()) {
				BigDecimal priceAdult = room.getPrice().getAdult().multiply(new BigDecimal(qtyDays)).multiply(new BigDecimal(qtyAdult));
				BigDecimal priceChild = room.getPrice().getChild().multiply(new BigDecimal(qtyDays)).multiply(new BigDecimal(qtyChildren));
				BigDecimal priceAdultPerDay = room.getPrice().getAdult();
				BigDecimal priceChildPerDay = room.getPrice().getChild();
				BigDecimal comission = BigDecimal.ZERO.add(priceAdult).add(priceChild);
				comission = comission.multiply(COMISSION);
				BigDecimal totalPrice = BigDecimal.ZERO.add(priceAdult).add(priceChild).add(comission);
				
				RoomPriceDetailDto priceDetail = RoomPriceDetailDto.builder()
						.comissionPerDayAdult(priceAdultPerDay.multiply(COMISSION))
						.pricePerDayAdult(priceAdultPerDay)
						.comissionPerDayChild(priceChildPerDay.multiply(COMISSION))
						
						.pricePerDayChild(priceChildPerDay)
						.build();
				
				RoomResultSearchDto roomResult = RoomResultSearchDto.builder()
						.categoryName(room.getCategoryName())
						.roomID(room.getRoomID())
						.comission(comission)
						.comissionApplied(COMISSION)
						.totalPrice(totalPrice)
						.priceDetail(priceDetail)
						.build();
				rooms.add(roomResult);
			}
			
			HotelResultSearchDto hotelResult = HotelResultSearchDto.builder()
					.cityName(hotel.getCityName())
					.id(hotel.getId())
					.name(hotel.getName())
					.rooms(rooms)
					.build();
			
			response.add(hotelResult);
		}
		return response;
	}

}
