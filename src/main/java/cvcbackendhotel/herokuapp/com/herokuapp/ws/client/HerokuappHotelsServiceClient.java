package cvcbackendhotel.herokuapp.com.herokuapp.ws.client;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.FeignClientProperties.FeignClientConfiguration;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import cvcbackendhotel.herokuapp.com.herokuapp.dto.HotelDto;

@FeignClient(name = "herokuapp-hotels", url = "https://cvcbackendhotel.herokuapp.com", configuration = FeignClientConfiguration.class)
public interface HerokuappHotelsServiceClient {

	@RequestMapping(value = "/hotels/avail/{cityId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<HotelDto> findHotelsByCityId(@PathVariable("cityId") Integer cityId);
	
	@RequestMapping(value = "/hotels/{hotelId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<HotelDto> findHotelDetailsById(@PathVariable("hotelId") Integer hotelId);
	
}