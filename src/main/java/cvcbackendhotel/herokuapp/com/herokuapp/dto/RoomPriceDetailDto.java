package cvcbackendhotel.herokuapp.com.herokuapp.dto;

import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RoomPriceDetailDto {

	private BigDecimal pricePerDayAdult;
	private BigDecimal comissionPerDayAdult;
	private BigDecimal pricePerDayChild;
	private BigDecimal comissionPerDayChild;

}