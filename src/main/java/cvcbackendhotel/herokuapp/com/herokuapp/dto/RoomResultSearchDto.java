package cvcbackendhotel.herokuapp.com.herokuapp.dto;

import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RoomResultSearchDto {

	private Long roomID;
	private String categoryName;
	private BigDecimal totalPrice;
	private BigDecimal comission;
	private BigDecimal comissionApplied;
	private RoomPriceDetailDto priceDetail;

}