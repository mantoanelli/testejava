package cvcbackendhotel.herokuapp.com.herokuapp.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RoomDto {

	private Long roomID;
	private String categoryName;
	private RoomPriceDto price;

}