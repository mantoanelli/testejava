package cvcbackendhotel.herokuapp.com.herokuapp.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class HotelDto {

	private Integer id;
	private String name;
	private Integer cityCode;
	private String cityName;
	private List<RoomDto> rooms;

}