package cvcbackendhotel.herokuapp.com.herokuapp.config;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.NOT_ACCEPTABLE;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.HttpStatus.UNAUTHORIZED;
import static org.springframework.http.HttpStatus.UNPROCESSABLE_ENTITY;
import static org.springframework.http.HttpStatus.UNSUPPORTED_MEDIA_TYPE;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMethod;

import com.fasterxml.classmate.TypeResolver;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.ResponseMessage;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

	@Value("${info.app.version}")
	private String version;

	@Value("${info.app.name}")
	private String name;

	@Value("${info.app.description}")
	private String description;

	@Autowired
	private TypeResolver typeResolver;

	@Bean
	public Docket newsApi() {

		final List<ResponseMessage> baseStatusCodes = Arrays.asList(getResponseMessage(OK), getResponseMessage(BAD_REQUEST, "ErrorResponse"), getResponseMessage(UNAUTHORIZED), getResponseMessage(FORBIDDEN), getResponseMessage(NOT_FOUND), getResponseMessage(NOT_ACCEPTABLE), getResponseMessage(UNSUPPORTED_MEDIA_TYPE), getResponseMessage(UNPROCESSABLE_ENTITY, "ErrorResponse"),
				getResponseMessage(INTERNAL_SERVER_ERROR));

		return new Docket(DocumentationType.SWAGGER_2).groupName("Herokuapp").apiInfo(apiInfo()).select().apis(RequestHandlerSelectors.basePackage("cvcbackendhotel.herokuapp.com.herokuapp")).build().useDefaultResponseMessages(false).additionalModels(typeResolver.resolve(ErrorResponse.class)).globalResponseMessage(RequestMethod.GET, baseStatusCodes)
				.globalResponseMessage(RequestMethod.PUT, baseStatusCodes).globalResponseMessage(RequestMethod.POST, baseStatusCodes).globalResponseMessage(RequestMethod.DELETE, baseStatusCodes);
	}

	private ApiInfo apiInfo() {
		return new ApiInfoBuilder().title(name).description(description).contact(new Contact("OPAH IT consulting", "https://www.opah.com.br", null)).version(version).build();
	}

	private ResponseMessage getResponseMessage(final HttpStatus httpStatus) {
		return getResponseMessage(httpStatus, null);
	}

	private ResponseMessage getResponseMessage(final HttpStatus httpStatus, final String responseModel) {
		return new ResponseMessageBuilder().code(httpStatus.value()).message(httpStatus.getReasonPhrase()).responseModel(StringUtils.isEmpty(responseModel) ? null : new ModelRef(responseModel)).build();
	}

}
