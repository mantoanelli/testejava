package cvcbackendhotel.herokuapp.com.herokuapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@EnableFeignClients(basePackages = { "cvcbackendhotel.herokuapp.com.herokuapp" })
@SpringBootApplication
public class HerokuappApplication {

	@RequestMapping("/")
	public String home() {
		return "redirect:/swagger-ui.html";
	}

	public static void main(String[] args) {
		SpringApplication.run(HerokuappApplication.class, args);
	}

}
