package cvcbackendhotel.herokuapp.com.herokuapp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cvcbackendhotel.herokuapp.com.herokuapp.dto.HotelDto;
import cvcbackendhotel.herokuapp.com.herokuapp.dto.HotelResultSearchDto;
import cvcbackendhotel.herokuapp.com.herokuapp.dto.HotelSearchDto;
import cvcbackendhotel.herokuapp.com.herokuapp.service.HotelsService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.log4j.Log4j2;

@Log4j2
@RestController
@RequestMapping(path = "/hotels", produces = { MediaType.APPLICATION_JSON_VALUE })
public class HotelsController {

	@Autowired
	private HotelsService service;

	@CrossOrigin
	@GetMapping("/avail/{cityId}")
	@ApiOperation(value = "Busca de hotéis por cidade", nickname = "findHotelsByCityId")
	public List<HotelDto> findHotelsByCity(@PathVariable Integer cityId) {
		log.info("BEGIN findHotelsByCityId - cityId={}", cityId);
		final List<HotelDto> response = this.service.findHotelsByCityId(cityId);
		log.info("END findHotelsByCityId - response: {}", response);
		return response;
	}

	@CrossOrigin
	@GetMapping("/{hotelId}")
	@ApiOperation(value = "Busca detalhes do hotel", nickname = "findHotelDetailsById")
	public List<HotelDto> findHotelDetailsById(@PathVariable Integer hotelId) {
		log.info("BEGIN findHotelDetailsById - hotelId={}", hotelId);
		final List<HotelDto> response = this.service.findHotelDetailsById(hotelId);
		log.info("END findHotelDetailsById - response: {}", response);
		return response;
	}
	
	@CrossOrigin
	@PostMapping("/search")
	@ApiOperation(value = "Buscar hotéis com cálculo de valores com comissão", nickname = "findHotelsTaxCalc")
	public List<HotelResultSearchDto> protocolValidation(@RequestBody HotelSearchDto data) throws Exception {
		log.debug("BEGIN findHotelsTaxCalc params={}", data);
		final List<HotelResultSearchDto> response = this.service.findHotelsTaxCalc(data);  
		log.debug("END findHotelsTaxCalc");
		return response;
	}
	
}
